package jp.alhinc.masubuchi_nanao.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
//		System.out.println("ここにあるファイルを開きます => " + args[0]);

		//コマンドライン引数が1つでない場合
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//[支店コード, 支店名]
		HashMap<String, String> branchmap = new HashMap<>();
		//[支店コード, 売上金額]
		HashMap<String, Long> rcdmap = new HashMap<>();
		//「売上集計フォルダ」を参照
		if(!input(args[0], "branch.lst", branchmap, rcdmap)) {
			return;
		}
		File rcd = new File(args[0]);
		File[] filename = rcd.listFiles();
		//売上ファイルだけをリストにする
		ArrayList<String> rcdlist = new ArrayList<>();
		for(int a=0; a<filename.length; ++a) {
//			ファイルの名前が"8桁+.rcd"でかつ普通のファイルであるか
		    if(filename[a].getName().matches("[0-9]{8}.rcd") && filename[a].isFile()){
//		    	System.out.println(filename[a].getName());
		    	rcdlist.add(filename[a].getName());
//		    	System.out.println(filename[a].getName());
//				System.out.println(rcdlist);
		    }
		}
		for(int i=0; i<rcdlist.size(); i++) {
		//拡張子を除く8桁の数字をString型→数値型に変換
			if(i+1<rcdlist.size()) {
				String currentstr = rcdlist.get(i).substring(0, 8);
	    		String nextstr = rcdlist.get(i+1).substring(0, 8);
//					System.out.println(cusrrentstr);
//					Syatem.out.println(nextstr);
	    		long currentln = Long.parseLong(currentstr);
				long nextln = Long.parseLong(nextstr);
//			    	System.out.println(currentln);
//			    	System.out.println(nextln);
				//売上ファイルの数字8桁が連番になっていない場合
				if(nextln - currentln != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
		}
		BufferedReader reader = null;
		//売上ファイル名を読む
		for(int i=0; i<rcdlist.size(); i++) {
			try {
				File record = new File(args[0],rcdlist.get(i));
				//売上ファイルの中身を読む
				FileReader filereader = new FileReader(record);
				reader = new BufferedReader(filereader);
				//[支店コード, 金額]リスト
				ArrayList<String> datalist = new ArrayList<>();
				String data;
				//ファイルの中身を一行ずつ読む
				while((data = reader.readLine())!= null) {
					datalist.add(data);
				}
//				System.out.println(datalist);
				//売上ファイルが3行以上ある場合
				if(datalist.size() != 2) {
					System.out.println(rcdlist.get(i) +"のフォーマットが不正です");
					return;
				}
				//それぞれの売上ファイルの2行目に数字以外が含まれている場合
				if(!datalist.get(1).matches("[0-9]*")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上ファイルの支店コードがbranch.lstに存在しない場合
				if(!branchmap.containsKey(datalist.get(0))){
					System.out.println(rcdlist.get(i) +"の支店コードが不正です");
					return;
				}
				//支店の合計金額を求める
				//すでにマップに加算されていた金額
				long rcdvalue = rcdmap.get(datalist.get(0));
//					System.out.println(rcdvalue);
				//String型として読み込んだ金額をLong型にする
				long ln = Long.parseLong(datalist.get(1));
//					System.out.println(ln);
				//金額が10桁以上の場合
				long sales = ln + rcdvalue;
//					System.out.println(sales);
//					System.out.println(ln + rcdvalue);
				if(String.valueOf(sales).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				rcdmap.put(datalist.get(0), sales);
//			System.out.println(rcdmap.keySet());
//			System.out.println(rcdmap.values());
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(reader != null) {
					try {
						reader.close();
					}catch(IOException e2) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
//		System.out.println(rcdmap.entrySet());
//		System.out.println(branchmap.entrySet());

//		System.out.println("支店コード：" + branchmap.keySet());
//		System.out.println("支店名　　：" + branchmap.values());
//		System.out.println("合計金額　：" + rcdmap.values());

		if(!output(args[0], "branch.out", branchmap, rcdmap)) {
			return;
		}
	}
	public static boolean input (String pass, String fileName, HashMap<String, String> branchmap, HashMap<String, Long> rcdmap) {
		BufferedReader br = null;
		try {
			//支店定義ファイルを読み込む
			File file = new File(pass, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			//支店定義ファイルの中を一行ずつ読む
			String line;
			while((line = br.readLine())!= null) {
				String[] branch = line.split(",");
//				System.out.println(branch[0]);
//				System.out.println(branch[1]);
				//支店定義ファイル1行の要素数が2つ || 一つ目の要素が3桁の数字でない場合
				if(branch.length != 2 || !branch[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchmap.put(branch[0], branch[1]);
				rcdmap.put(branch[0], (long) 0);
			}
		}catch(IOException e) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	public static boolean output(String dirpass, String fileName, HashMap<String, String>branchmap, HashMap<String, Long> rcdmap) {
		BufferedWriter bw = null;
		try {
			//出力する
			File File = new File(dirpass, fileName);
			FileWriter fw = new FileWriter(File);
			bw = new BufferedWriter(fw);
			for(HashMap.Entry<String, String> entry : branchmap.entrySet()) {
//				System.out.println(entry.getKey() + "," + entry.getValue() + "," + rcdmap.get(entry.getKey()));
				bw.write(entry.getKey() + "," + entry.getValue() + "," + rcdmap.get(entry.getKey()) + "\r\n");
			}
		}catch(IOException ex) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e2) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

